import { Component, ChangeDetectorRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  duration: number = 10;
  animationRunning: boolean = true;

  constructor(private cdr: ChangeDetectorRef) {}

  onDurationChange(event: any): void {
    this.duration = event.target.value;
  }

  updateAnimationDuration(): string {
    return this.duration + 's';
  }

  startAnimation(): void {
    this.animationRunning = false;
    this.cdr.detectChanges();

    setTimeout(() => {
      this.animationRunning = true;
      this.cdr.detectChanges();
    }, 10);
  }

  animationEnded(): void {
    this.animationRunning = false;
  }
}
